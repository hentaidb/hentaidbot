package i18n

import (
	"os"
	"path/filepath"
	"strings"

	log "github.com/kirillDanshin/dlog"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"golang.org/x/text/language"
	"gopkg.in/yaml.v2"
)

type Bundle struct{ *i18n.Bundle }

var Translator *Bundle

func Open(path string) (b *Bundle, err error) {
	bundle := i18n.NewBundle(language.English)
	bundle.RegisterUnmarshalFunc("yaml", yaml.UnmarshalStrict)

	err = filepath.Walk(path, func(path string, file os.FileInfo, err error) error {
		log.Ln("walk into", path)
		if !strings.HasSuffix(file.Name(), ".all.yaml") {
			return err
		}

		_, err = bundle.LoadMessageFile(path)
		return err
	})

	return &Bundle{Bundle: bundle}, err
}
