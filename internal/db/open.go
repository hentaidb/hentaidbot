package db

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type DataBase struct{ *gorm.DB }

var DB *DataBase

func Open(path string) (db *DataBase, err error) {
	c, err := gorm.Open("sqlite3", path)
	if err != nil {
		return nil, err
	}

	c.SingularTable(false)
	c.LogMode(true)

	if err = c.DB().Ping(); err != nil {
		return nil, err
	}

	db = &DataBase{DB: c}
	return
}
