package config

import (
	"path/filepath"

	log "github.com/kirillDanshin/dlog"
	"github.com/spf13/viper"
	"gitlab.com/hentaidb/robot/telegram/internal/errors"
)

var (
	Config         = new(viper.Viper)
	ErrInvalidPath = errors.New("invalid path to config file")
)

// Open just open configuration file for parsing some data in other functions
func Open(path string) (cfg *viper.Viper, err error) {
	log.Ln("Opening config on path:", path)

	dir, file := filepath.Split(path)
	ext := filepath.Ext(file)
	if file == "" || ext == "" {
		return nil, ErrInvalidPath
	}

	fileExt := ext[1:]
	fileName := file[:(len(file)-len(fileExt))-1]

	log.Ln("dir:", dir)
	log.Ln("file:", file)
	log.Ln("fileName:", fileName)
	log.Ln("fileExt:", fileExt)

	cfg = viper.New()

	cfg.AddConfigPath(dir)
	cfg.SetConfigName(fileName)
	cfg.SetConfigType(fileExt)

	log.Ln("Reading", file)
	err = cfg.ReadInConfig()

	return
}
